<?php

namespace Polcode\EduBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Polcode\EduBundle\Entity;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('PolcodeEduBundle:Default:index.html.twig', array('name' => $name));
    }
    
    /**
     * @Template()
     */
    public function testAction() {
        $tag1 = new Entity\Tag();
        $tag1->setTag("tag1");
        $tag2 = new Entity\Tag();
        $tag2->setTag("tag2");
        
        $category = new Entity\Category();
        $category->setName("Category1");
        
        $product1 = new Entity\Product();
        $product1->setCategory($category);
        $product1->setName("Product1");
        $product1->setCreatedAt(new \DateTime());
        $product1->setModifiedAt(new \DateTime());
        
        $product1->addTag($tag1);
        $product1->addTag($tag2);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($tag1);
        $em->persist($tag2);
        $em->persist($category);
        $em->persist($product1);
        $em->flush();
        
        $data = array();
        return $data;
    }
}
