<?php

namespace Polcode\EduBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Product
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime")
     */
    private $modifiedAt;
    
    /**
     * @var Category
     * 
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    /**
     * @var ArrayCollection 
     *
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="product_tag",
     *      joinColumns={
     *          @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     *      }
     * )
     */
    private $tags;

    public function __construct() {
        $this->tags = new ArrayCollection();
        $this->setCreatedAt(new \DateTime() );
        $this->setModifiedAt(new \DateTime() );
    }
    
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdateAt()
    {
        $this->setModifiedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Product
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set category
     *
     * @param \Polcode\EduBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\Polcode\EduBundle\Entity\Category $category)
    {
        $this->category = $category;
        $category->addProduct($this);

        return $this;
    }

    /**
     * Get category
     *
     * @return \Polcode\EduBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add tags
     *
     * @param \Polcode\EduBundle\Entity\Tag $tag
     * @return Product
     */
    public function addTag(\Polcode\EduBundle\Entity\Tag $tag)
    {
        if( !$this->tags->contains($tag) ) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \Polcode\EduBundle\Entity\Tag $tags
     */
    public function removeTag(\Polcode\EduBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }
    
    public function __toString() {
        return $this->getName();
    }
}
